from libraries.Common import Common

from datetime import date, datetime, timedelta

from program.Dfp import Dfp
from program.Ooyala import Ooyala
from program.DBImport import DBImport

date_format = r"%Y-%m-%d"

def main():
    dbimport = DBImport()

    start = datetime.strptime('2018-01-01', date_format)
    end = (datetime.today() - timedelta(1))
    diff = end - start
    days = diff.days+1

    brands = dbimport.config.get('brands').keys()
    for brand in brands:
        opts = dbimport.config.get('brands.%s'%brand)
        group = dbimport.config.get('groups.%s'%opts['group'])
        if 'validate_mysql' in group :

            for i in range(days):
                current_date = (start+timedelta(days=i)).strftime(date_format)
                query_mysql = group['validate_mysql'] % (current_date, current_date)
                count_mysql = int(dbimport.run_mysql(brand, query_mysql))

                query_psql = "SELECT COUNT (DISTINCT nota_id) FROM test_xalok WHERE brand = '%s' AND first_published_at >= '%s' AND first_published_at <= '%s'" % (brand, current_date, current_date)
                count_psql = int(dbimport.run_psql(query_psql))

                if count_mysql != count_psql:
                    print(brand, current_date ,count_mysql, count_psql)
        



if __name__ == '__main__':
    main()
    exit(0)
