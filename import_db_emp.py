from subprocess import call
import io,sys,os
from datetime import date, timedelta

groups = {
    'xalok': {
        'host' : 'aurora01-replica.clz6pendayku.us-east-1.rds.amazonaws.com',
        'port' : '3306',
        'user' : 'videoreportes',
        'password' : 'eeCh8Eheijiehi'
    },
    'perucom' : {
        'host' : '54.81.254.106',
        'port' : '3307',
        'user':'videoreportes',
        'password':'eeCh8Eheijiehi'
    },
    'publimetro' : {
        'host' : '174.129.223.60',
        'port' : '3307',
        'user':'videoreportes',
        'password':'eeCh8Eheijiehi'
    }
}
brands = {
    'elcomercio' : {
        'group' : 'xalok',
        'dbname' : 'elcomerciodb',
    },
    'depor' : {
        'group' : 'xalok',
        'dbname' : 'depordb',
    },
    'trome' : {
        'group' : 'xalok',
        'dbname' : 'ecperu',
    },
    'trome' : {
        'group' : 'xalok',
        'dbname' : 'gestiondb',
    },
    'peru21' : {
        'group' : 'xalok',
        'dbname' : 'peru21db',
    },
    'gestion' : {
        'group' : 'xalok',
        'dbname' : 'gestiondb',
    },
    'perucom' : {
        'group' : 'perucom',
        'dbname' : 'md3perucom3db',
    },
    'laprensa' : {
        'group' : 'perucom',
        'dbname' : 'md3laprensadb',
        'query' : 'laprensa'
    },
    'publimetro' : {
        'group' : 'publimetro',
        'dbname' : 'md3publimetro2db'
    }
}

productos_csv = """elcomercio;tag;en-vivo;EL COMERCIO EN VIVO
gestion;tag;con-las-cuentas-claras;GTV: CON LAS CUENTAS CLARAS
gestion;tag;gestion-tv-con-las-cuentas-claras-siglo-bpo;GTV: CON LAS CUENTAS CLARAS
gestion;tag;hablemos-mas-simple;GTV: HABLEMOS MÁS SIMPLE
gestion;tag;hablemosmas-simple-con-asbanc;GTV: HABLEMOS MÁS SIMPLE
gestion;tag;hablemos-mas-simple-con-asbanc;GTV: HABLEMOS MÁS SIMPLE
gestion;tag;20-en-empleabilidad;GTV: 20 EN EMPLEABILIDAD
gestion;tag;consultorio-de-negocios;GTV: CONSULTORIO DE NEGOCIOS
gestion;tag;trabajo-en-accion;GTV: TRABAJO EN ACCIÓN
gestion;tag;gestion-tv-trabajo-en-accion-mtpe-gratificaciones;GTV: TRABAJO EN ACCIÓN
gestion;tag;gestion-en-vivo;GESTIÓN EN VIVO
depor;tag;negro-y-blanco;NEGRO Y BLANCO
depor;tag;negro-y-blancofutbol-peruano;NEGRO Y BLANCO
depor;tag;hinchada-hay-una-sola;HINCHADA HAY UNA SOLA
depor;tag;youtubehinchada-hay-una-sola;HINCHADA HAY UNA SOLA
depor;tag;top-10;TOP 10
depor;tag;la-palabra-rusa;LA PALABRA RUSA
depor;tag;cuy-yimi;EL CUY YIMI
depor;tag;en-vivo;DEPOR EN VIVO
trome;category;emprende-trome;EMPRENDE TROME
trome;tag;emprende-trome;EMPRENDE TROME
trome;category;dr-trome;DR. TROME
trome;tag;dr-trome;DR. TROME
trome;tag;de-dietas-y-mas;DIETAS Y MÁS
trome;tag;horoscopo;HORÓSCOPO
trome;tag;horoscopo-chino-2017;HORÓSCOPO
trome;category;horoscopo;HORÓSCOPO
trome;tag;trome-en-vivo;TROME EN VIVO
perucom;tag;envivo;PERÚ.COM EN VIVO
publimetro;tag;envivo;PUBLIMETRO EN VIVO"""

products = "UNION ALL ".join("SELECT '%s'brand,'%s'type,'%s'slug,'%s'nombre "%tuple(l.split(';')) for l in productos_csv.split("\n"))

file_out_path = r"C:\\Users\\pc\\data\\%s-%s-%s.csv"
file_queries_path = r"C:\Users\pc\Desktop\queries"

s3_bucket = 's3n://appvideos-ec/xalok'

def call_sqoop(brand, date):
    opts = brands[brand]
    group = groups[opts['group']]

    file_query_path = r"%s\query_%s.sql"%(file_queries_path, 'query' in opts and opts['query'] or opts['group'])
    query = io.open(file_query_path, mode="r", closefd=True, encoding="utf-8").read()
    for find,replace in {'@brand':brand,'@start-date':start_date,'@end-date':end_date,'@products':products}.items():
        query = query.replace(find,replace)

    params = (
        'sqoop',
        'import'
        '--connect jdbc:mysql://%s/%s' % (group['host'], opts['dbname']),
        '--username %s' % group['user'],
        '--password %s' % group['password'],
        r'--query "%s and  \$CONDITIONS"' % query,
        '-m 1',
        '--split-by page.id',
        '--target-dir "%s/raw/fact_videos/%s/%s"' % (s3_bucket, brand, date),
        "--fields-terminated-by '\t'",
        "--lines-terminated-by '\n'",
        "--null-string '\\N'",
        "--null-non-string '\\N'",
        "--compression-codec snappy"
        )
    quit(" ".join(params))

def download_data(brand, start_date, end_date):
    opts = brands[brand]
    group = groups[opts['group']]

    file_query_path = r"%s\query_%s.sql"%(file_queries_path, 'query' in opts and opts['query'] or opts['group'])
    
    query = io.open(file_query_path, mode="r", closefd=True, encoding="utf-8").read()
    for find,replace in {'@brand':brand,'@start-date':start_date,'@end-date':end_date,'@products':products}.items():
        query = query.replace(find,replace)

    f =  io.open(file_out_path % (brand,start_date,end_date), mode="w")
    params = (
        'mysql',
        '-h%s' % group['host'],
        '-P%s' % group['port'],
        '-u%s' % group['user'],
        '-p%s' % group['password'],
        '-D%s' % opts['dbname'],
        "-s",
        '-N',#--skip-column-names
        '-r',
        "--binary-mode",
        "--default-character-set=utf8",
        '-e %s'%query,
        )
    # quit(params)
    print("Downloading %s from %s to %s..." % (brand,start_date,end_date))
    call(params, stdout=f)
    f.close()

def upload_data(brand, start_date, end_date):
    opts = brands[brand]
    group = groups[opts['group']]

    file = file_out_path % (brand,start_date,end_date)
    
    query = "DELETE FROM page WHERE page.brand = '@brand' AND DATE_FORMAT(page.first_published_at,'%%Y-%%m-%%d') <= '@start-date' AND DATE_FORMAT(page.first_published_at,'%%Y-%%m-%%d') >= '@end-date'; LOAD DATA INFILE '%s' IGNORE  INTO TABLE page CHARACTER SET UTF8 FIELDS TERMINATED BY ';' ENCLOSED BY '' LINES TERMINATED BY '\\n';" % file
    query = query.replace('@brand',brand).replace('@start-date',start_date).replace('@end-date',end_date)

    params = (
        'mysql',
        '-uroot',
        # '-p',
        '-Dvideos',
        "-s",
        "--default-character-set=utf8",
        '-e %s'%query,
    )
    print("Uploading %s from %s to %s..." % (brand,start_date,end_date))
    call(params)

date_format = r"%Y-%m-%d"

if __name__ == "__main__":
    argv = sys.argv[1:]
    args = dict(zip(argv[::2],argv[1::2]))

    yesterday = (date.today() - timedelta(1)).strftime(date_format)
    
    start_date = '-start-date' in args and args['-start-date'] or yesterday
    end_date = '-end-date' in args and args['-end-date'] or yesterday

    if('-db' in args):
        download_data(args['-db'], start_date, end_date)
        upload_data(args['-db'], start_date, end_date)
    else:
        for brand in brands.keys():
            download_data(brand, start_date, end_date)
            upload_data(brand, start_date, end_date)