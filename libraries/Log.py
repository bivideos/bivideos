# -*- coding: utf-8 -*-

import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)

import sys
from datetime import datetime
import tempfile
import os
import inspect
import smtplib
import time
from datetime import date
from os import path
from libraries.Config import Config


BASE_DIR = os.path.normpath(os.path.dirname(os.path.abspath(__file__))+'/..')


class Log(object):

    config = None
    log_installed = False

    # Singleton
    __instance = None

    @staticmethod
    def instance():
        if Log.__instance is None:
            Log.__instance = Log()
        return Log.__instance

    @staticmethod
    def error(ex, end=True):
        logger = logging.getLogger(__name__)
        if isinstance(ex, str):
            logging.error(ex)
        else:
            logger.exception(ex)

        if not end:
            return

        # return#for test

        try:
            subject = 'Appvideos: '+str(ex)
            message = str(ex)
            if Log.log_installed:
                message = open(Log.log_installed, "r").read()
            log = Log.instance()
            log.send_email(subject, message)
        except Exception, ex:
            logging.error("Error sending email:\n%s\n" % str(ex))
        exit(1)

    def __init__(self):
        Log.config = Config('log')
        self.setup()

    @property
    def log_dir(self):
        log_dir = Log.config.get('log_dir', False)
        if not log_dir:
            log_dir = os.path.join(BASE_DIR, 'data', 'log')
        return log_dir

    def setup(self):
        if Log.log_installed:
            return

        today = date.today().strftime('%Y%m%d')
        now = int(time.time())
        full_path = os.path.join(self.log_dir, '%s.%s.log' % (today, now))
        if not os.path.isdir(self.log_dir):
            os.makedirs(self.log_dir)

        root_log = logging.getLogger()
        for h in root_log.handlers:
            root_log.removeHandler(h)

        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s:%(levelname)s:%(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S',
                            filename=full_path,
                            filemode='w')

        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.INFO)
        formatter = logging.Formatter(
            '%(asctime)s:%(levelname)s:%(message)s', '%Y-%m-%d %H:%M:%S')
        console.setFormatter(formatter)
        root_log.addHandler(console)

        print('Logging in: %s' % full_path)
        Log.log_installed = full_path

    def send_email(self, subject, message_text):

        email_to = self.config.get('email_to', False)
        if not email_to:
            return logging.info("Email configuration 'email_to' not provided.")

        try:
            from httplib2 import Http
            from googleapiclient.discovery import build
            from oauth2client import file
            from email.mime.text import MIMEText
            from googleapiclient import errors
            import base64
        except Exception as ex:
            return logging.warn("Library google-api-python-client not found. install python -m pip install google-api-python-client")

        credential_path = path.join(self.config.dir, 'credentials_email.json')
        store = file.Storage(credential_path)
        creds = store.get()
        if not creds or creds.invalid:
            return logging.error("The email credentials supplied were not correct: %s" % credential_path)

        mime = MIMEText(message_text)
        to_addr_list = email_to.split(',')
        mime['to'] = ','.join(to_addr_list)
        #mime['from'] = sender
        mime['subject'] = subject
        message = {'raw': base64.urlsafe_b64encode(mime.as_string())}

        try:
            service = build('gmail', 'v1', http=creds.authorize(Http()))
            message = (service.users().messages().send(
                userId='me', body=message).execute())
            logging.debug('Message Id: %s' % message['id'])
            logging.info("Email enviado.")
        except errors.HttpError, error:
            logging.exception(error)


Log.instance()
