# -*- coding: utf-8 -*-

import logging
from libraries.Log import Log
import subprocess
class SubProcess(object):

    @staticmethod
    def call(params=[], stdin=None, stdout=subprocess.PIPE):
        
        try:
            logging.debug("Ejecutando el comando: %s" % " ".join(params))
            proc = subprocess.Popen(params, stdout=stdout, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            _stdout,_stderr = proc.communicate(stdin)
            if proc.returncode != 0 :
                Log.error(u"Ocurrió un error mientras se ejecutaba el comando %s: %s" % (params[0], _stderr))
            return _stdout
        except Exception as ex:
            Log.error("Error al ejecutar el comando:\n%s\nError: %s\n\n" % (params[0], str(ex)))