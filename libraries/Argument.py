# -*- coding: utf-8 -*-

from libraries.Log import Log
from datetime import date, datetime, timedelta
import argparse


class Argument(object):

    date_format = r"%Y%m%d"

    _args = None
    _parser = None

    def __init__(self):
        self._parser = argparse.ArgumentParser()
        # self._read()

    def __getattr__(self, item):
        
        # Cargar los argumentos si aún no se ha hecho
        if self._args is None:
            self._args = self._read()

        # Buscar el argumento solicitado
        if self.__dict__.has_key(item):
            return self.__dict__[item]
        elif self._args.has_key(item):
            return self._args[item]
        else:
            Log.error(u"El parámetro de consola '%s' no fue encontrado en %s." %
                          (item, str(self._args)))
            raise AttributeError(item)

    def _read(self):

        self._parser.add_argument('--start_date', help=u'Fecha de inicio inclusivo a extraer Formato %s. Si no se proporciona y tampoco se usa --date, se considera el día de ayer' %
                                 self.date_format, type=str, required=False, default='yesterday')
        self._parser.add_argument('--end_date', help=u'Fecha de fin  inclusivo a extraer. Formato %s. Si no se proporciona y tampoco se usa --date, se considera el día de ayer' %
                                 self.date_format, type=str, required=False, default='yesterday')
        self._parser.add_argument('--date', help=u'Fecha de data a extraer. Formato %s. Para extraer la data de 1 solo día' %
                                 self.date_format, type=str, required=False, default='yesterday')
        self._parser.add_argument('--brand', help='Filtro por marca a extraer, si no se define realiza todas las marcas.',
                                 type=str, required=False, default='all')
        
        self._parser.add_argument('--force_level', help=u'Nivel de fuerza de actualización. Del 0 al 3. Predeterminadamente no sobreescribe ningún dato existente.',
                                 type=int, required=False, default=0)
        # print(self._parser.format_help())
        yesterday = (date.today() - timedelta(1)).strftime(self.date_format)

        args = dict(self._parser.parse_args()._get_kwargs())

        # Obtener el rango de fechas solicitadas
        if not args['date'] == 'yesterday':
            start_date = args['date']
            end_date = args['date']
        else:
            args['date'] = yesterday
            start_date = args['start_date'] if not args['start_date'] == 'yesterday' else yesterday
            end_date = args['end_date'] if not args['end_date'] == 'yesterday' else yesterday

        self._validate_dates(start_date, end_date)

        args['date'] = datetime.strptime(args['date'], self.date_format)
        args['start_date'] = datetime.strptime(start_date, self.date_format)
        args['end_date'] = datetime.strptime(end_date, self.date_format)
        
        if int(args['force_level']) > 0:
            from libraries.Common import Common
            Common.force_level = int(args['force_level'])
            
        return args

    def add_argument(self, *args, **kwargs):
        self._parser.add_argument(*args, **kwargs)

    def _validate_dates(self, start_date, end_date):
        # Validar el formato rango de fechas solicitado
        if not start_date or not end_date:
            Log.error(u'No se envió ningún rango de fechas.')
        else:
            try:
                datetime.strptime(start_date, self.date_format).strftime(
                    self.date_format)
            except:
                Log.error(
                    'La fecha seleccionada es incorrecta. Usar el formato %s.' % self.date_format)
            try:
                datetime.strptime(end_date, self.date_format).strftime(
                    self.date_format)
            except:
                Log.error(
                    'La fecha seleccionada es incorrecta. Usar el formato %s.' % self.date_format)

        # Validar que los rangos de fechas estén dentro de los límites
        today = date.today().strftime(self.date_format)

        if(start_date > end_date):
            Log.error('Fecha de fin debe ser mayor o igual a la fecha de inicio: (%s > %s).' % (
                start_date, end_date))

        if(start_date > today):
            Log.error('Fecha de inicio no debe ser mayor a la fecha de hoy: (%s > %s).' % (
                start_date, today))

        if(end_date > today):
            Log.error('Fecha de fin no debe ser mayor a la fecha de hoy: (%s > %s)' % (
                end_date, today))
