# -*- coding: utf-8 -*-

import os
import csv
import logging
import io

BASE_DIR = os.path.normpath(os.path.dirname(os.path.abspath(__file__))+'/..')


class WritterCsv(object):

    _file = None
    _file_tmp = None
    
    _dict_reader = None
    _dict_writer = None

    def __init__(self, full_path, headers=None, delimiter=','):
        self._path = full_path
        self.headers = headers
        self.delimiter = delimiter


    @property
    def dir(self):
        dir = os.path.dirname(self._path)
        if not os.path.exists(dir):
            logging.debug("Creando nueva carpeta %s" % dir)
            os.makedirs(dir)
        return dir

    @property
    def full_path(self):
        self.dir
        return self._path

    @property
    def path_file(self):
        return self.full_path

    @property
    def path_file_tmp(self):
        return self.full_path+'.tmp'

    
    @property
    def file_tmp(self):
        if not self._file_tmp:
            # self._file_tmp = io.open(self.path_file_tmp, 'w', encoding="utf-8")
            self._file_tmp = open(self.path_file_tmp, mode='wb')
        return self._file_tmp
    
    @property
    def file(self):
        if not self._file:
            self._file = io.open(self.path_file, 'r', encoding="utf-8")
        return self._file

    
    @property
    def count_lines(self):
        lines = len(self.file.readlines())
        if self.headers:
            lines -= 1
        return lines

    @property
    def exists(self):
        return os.path.isfile(self.path_file)


    @property
    def dict_writer(self):
        if not self._dict_writer:
            self._dict_writer = csv.DictWriter(
                self.file_tmp, self.headers, delimiter=self.delimiter)
            self._dict_writer.writeheader()
        return self._dict_writer

    def writerow(self, row):
        return self.dict_writer.writerow(row)

    def writerows(self, rows):
        return self.dict_writer.writerows(rows)

    def writeraw(self, content):
        full_path = self.full_path
        logging.debug("Escribir en modo crudo al archivo %s" % full_path)
        try:
            f = self.file_tmp
            f.write(content)
            count = len(content.split("\n"))-1
            self.close()
            logging.debug('Se escribieron %s registros.' % count)
            return count
        except Exception as ex:
            logging.error("No se pudo escribir el archivo csv: %s\nError:%s" % (full_path, str(ex)))
            raise

    def close(self):
        # Cerrar los archivos
        if self._file: 
            self._file.close()
            self._dict_reader = None
            self._file = None

        # Si se ha usado el archivo temporal, cerrar y moverlo
        if self._file_tmp: 
            self._file_tmp.close()
            self._dict_writer = None
            self._file_tmp = None
        
            # Eliminar el archivo original si ya existe
            if os.path.isfile(self.path_file):
                os.remove(self.path_file)
        
            # Mover el archivo temporal al original
            os.rename(self.path_file_tmp, self.path_file)
