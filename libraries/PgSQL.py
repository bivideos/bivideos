# -*- coding: utf-8 -*-
from medoo.database.pgsql import Pgsql
from Config import Config
import io
import logging


class PgSQL(Pgsql):

    _config = None

    def __init__(self):
        if not PgSQL._config:
            PgSQL._config = Config('pgsql')
        super(PgSQL, self).__init__(**PgSQL._config.connection)

    def count(self, table, where=None, join=None, distinct=False, newtable=None, sub=None):
        rs = self.select(table, '*|count', where,
                         join, distinct, newtable, sub)
        count = rs.first().count
        logging.debug(u"Se obtuvo %d como conteo de la tabla %s con parámetros %s" % (count,table,str(where)) )
        return count

    def delete(self, table, where, commit=True):
        result = super(PgSQL, self).delete(table, where, commit)
        if result:
            result = self.cursor.rowcount
            logging.debug("Se eliminaron %s registros de la tabla %s con los criterios %s" % (result, table, str(where)))
        return result

    def upload_csv(self, table, path, delimiter='\t', null='NULL', encoding='utf8', header=False):
        f = io.open(path, 'r', encoding="utf-8")

        # Si se envio header, leer la primera linea del archivo
        if header:
            header_list = [l.strip() for l in f.readline().split(delimiter)]
            if header_list[0] == 'date':
                header_list[0] = 'start_date'
            header = "(%s)" % (",".join(header_list))
        
        sql = "COPY %s%s FROM STDIN WITH DELIMITER '%s' NULL '%s' ENCODING '%s' CSV" % (
            table, header if header else '', delimiter, null, encoding)

        logging.debug("Ejecutando query: %s" % sql)
        
        self.cursor.copy_expert(sql, f)
        rowcount = self.cursor.rowcount
        if rowcount:
            self.commit()
            logging.debug("Se copiaron %s registros de la tabla %s." % (rowcount, table))
        f.close()
        return rowcount

    def call_func(self, func_name, *args):
        logging.debug(u"Ejecutando función %s%s" % (func_name, str(args)))
        cur = self.cursor
        cur.callproc(func_name, args)
        result = cur.fetchone()
        self.commit()
        return result
        