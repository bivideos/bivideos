# -*- coding: utf-8 -*-

import json
import os
from os import path
import logging

BASE_DIR = path.normpath(path.dirname(path.abspath(__file__))+'/..')


class Dotable(dict):

    __getattr__ = dict.__getitem__

    def __init__(self, d):
        self.update(**dict((k, self.parse(v))
                           for k, v in d.iteritems()))

    @classmethod
    def parse(cls, v):
        if isinstance(v, dict):
            return cls(v)
        elif isinstance(v, list):
            return [cls.parse(i) for i in v]
        else:
            return v


class Config(object):
    # Directorio donde se encuentran los archivos de configuración
    _dir = path.join(BASE_DIR, 'config')


    def __init__(self, name):
        self._name = name
        self._config = {}        
        self._load()
        self._modified = False
        self.__initialised = True


    @property
    def _full_path(self):
        """ 
            Retorna la ruta completa del archivo de configuración.
        """
        return path.join(self._dir,self._name+'.json')
    
    @property
    def get_path(self):
        return self._dir

    def _load(self):
        """
            Carga un archivo de configuración tipo json
        """
        logging.debug(u"Cargando archivo de configuración: %s" % self._full_path)
        if path.isfile(self._full_path):
            try:
                with open(self._full_path) as cfg:
                    dictconfig = json.load(cfg)
                    self._config = Dotable.parse(dictconfig)
            except Exception as ex:
                logging.error(u"El archivo de configuración '%s' no es un json válido: %s" %
                            (self._full_path, ex.message))
        else:
            logging.warn("El archivo %s no existe." % self._full_path)

    

    def get(self, name=None, default=None):
        """
            Método publico para obtener un valor de configuración con un formato dot (.)
            Ej. config.public.dir.path
            Se usa un segundo parámetro 'default' a ser devuelto cuando la configuración no se encuentre.
            Sirve para obtener un detalle de algún error producto de una falta de variable de configuración.
            Sí no se envía un nombre se obtiene todo el archivo de configuración.
            Si No se encuentra el nombre enviado y no se ha enviado el valor default, se produce un error.        
        """
        config = self._config

        # Si no se solicito el nombre devolver todo el config.
        if name is None:
            return config

        # Si se solicito un path, leer del archivo el valor específico
        names = name.split('.')
        for n in names:
            try:
                config = getattr(config, n)
            except Exception as ex:
                if default is None:
                    logging.error(u"El parámetro de configuración '%s' no fue encontrado en %s." % (
                        name, self._full_path))
                else:
                    logging.debug(
                        u"Valor predeterminado del parámetro '%s' fue usado: %s" % (name, default))
                    config = default
                break  # for
        return config

    def rewrite(self):
        with open(self._full_path, 'w') as outfile:
            logging.debug(u"Guardando la configuración en el archivo %s", self._full_path)
            json.dump(self._config, outfile)
            return True

    def __getattr__(self, item):
        """
            Método mágico para obtener un valor de configuración en el archivo config.
        """
        # print('__getattr__', item)
        
        if self.__dict__.has_key(item):
            return self.__dict__[item]
        elif self._config.has_key(item):
            return self._config[item]
        else:
            logging.error(u"El parámetro de configuración '%s' no fue encontrado en %s" %
                          (item, self._full_path))
            raise AttributeError(item)

    def __setattr__(self, item, value):
        # print('__setattr__', item, value)
        
        if not self.__dict__.has_key('_Config__initialised'):  # this test allows attributes to be set in the __init__ method
            return dict.__setattr__(self, item, value)
        elif self.__dict__.has_key(item):       # any normal attributes are handled normally
            dict.__setattr__(self, item, value)
        else:
            logging.debug(u"Estableciendo la configuración %s=%s." % (item,value))
            self._config[item] = value
            self._modified = True

