# -*- coding: utf-8 -*-

from libraries.Config import Config
from libraries.Resource import Resource
import sys,logging,os,subprocess
from datetime import date, datetime, timedelta
from libraries.Log import Log
from libraries.PgSQL import PgSQL

class Common(object):
    
    date_format = r"%Y%m%d"

    force_level = 0
    """Nivel de fuerza de actualización
    Este valor establece que tan profundo se realizará la actualización de valores para cada proceso.
    Nivel 0: Nunca se actualizará nada existente.
    Nivel 1:
        Ooyala, Dfp, DBImport: 
            Actualizar la subida a la base de datos a partir de los csv que puedan existir.
    Nivel 2:
        Ooyala:
            Volver a generar el csv partir de los archivos pickle generados.
        Dfp:
            Volver a generar el csv a partir de los csv.gz generados.
    Nivel 3:
        Ooyala:
            Volver a realizar las solicitudes al API de ooyala y generar los archivos pickle.
        Dfp:
            Volver a realizar las solicitudes al API de Dfp y generar los archivos csv.gz.
        DBImport:
            Volver a generar el csv a partir de las bases de datos
    """
    
    # Cargar la configuración común una sola vez
    config = None

    
        
    def __init__(self):
        
        self.config = Config(self.__class__.__name__.lower())
        self.resources = Resource()
        self.psql_added = 0 #Contador de total de registros agregados

    def get_config(self,path):
        class_name = self.__class__.__name__.lower()
        return self.config.get(class_name+'.'+path)        

    _db = None
    @property
    def db(self):
        if not Common._db:
            Common._db = PgSQL()
        return Common._db

    def run_psql(self, query):

        dbname = self.config.psql.dbname
        host = self.config.psql.host
        user = self.config.psql.user
        port = self.config.psql.port
        
        params = (
            'psql',
            '-d%s'%dbname,
            '-h%s'%host,
            '-U%s'%user,
            '-w',
            '-t',
            '-p%s'%port, 
            '-c%s' % query
        )
        # print(" ".join(params))
        try:
            proc = subprocess.Popen(params, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout,stderr = proc.communicate()
            if proc.returncode != 0 :
                raise Exception("%s:%s: Se produjo un error al ejecutar la query:\n %s\nError: %s"% (datetime.now(), self.__class__.__name__,query, stderr))
            return stdout
        except Exception as ex:
            Log.error(Exception('No se pudo ejecutar: %s'%" ".join(params),ex))

    def run_mysql(self, brand, query):
        brands = self.config.brands
        opts = brands[brand]
        groups = self.config.groups
        group = groups[opts['group']]

        params = (
            'mysql',
            '-h%s' % group['host'],
            '-P%s' % group['port'],
            '-u%s' % group['user'],
            '-p%s' % group['password'],
            '-D%s' % opts['dbname'],
            "-s",
            '-N',#--skip-column-names
            '-r',
            "--binary-mode",
            "--default-character-set=utf8",
            '-e %s'%query,
        )

        try:
            proc = subprocess.Popen(params, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout,stderr = proc.communicate()
            if proc.returncode != 0 : 
                raise Exception("%s:%s: Se produjo un error al ejecutar la query:\n %s\nError: %s"% (datetime.now(), self.__class__.__name__, query, stderr))
            return stdout    
        except Exception as ex:
            Log.error(Exception('No se pudo ejecutar: %s'%" ".join(params),ex))            

    def init(self, current_date, brand = ''):
        pass
    