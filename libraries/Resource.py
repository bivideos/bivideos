# -*- coding: utf-8 -*-

import io,os, json, inspect, sys, logging


class Resource():
    
    __resources = {}
    __names = []
    __path = ''
    __last_load = ''

    def __init__(self, file_name=''):
        dir = os.path.dirname(inspect.getfile(self.__class__))
        self.__path = os.path.join(os.path.split(dir)[0], 'resources')

        if file_name :
            self.__load(file_name)
        

    def __load(self,file_name):

        self.__last_load = file_name

        if not file_name in self.__names:
            self.__names.append(file_name)

        data = None
        path = os.path.join(self.__path, file_name)
        if os.path.isfile(path):
            with io.open(path, 'r', encoding="utf-8") as cfg:
                try:
                    data = cfg.read()
                except Exception as e:
                    logging.info("Advertencia al leer archivo de recurso:\n%s -> %s\n" % (path, str(e)))                
        else: 
            logging.error("Archivo de recurso no encontrado: %s" % path)

        self.__resources[file_name] = data

        return self

    def get_path(self):
        return self.__path

    def get(self, file_name = ''):
        if not file_name : file_name = self.__last_load
        self.__load(file_name)

        return self.__resources[file_name]
        
        
        