


import sys
from program.Ooyala import Ooyala
from datetime import date, datetime, timedelta
import csv,os,io,sys,logging,pickle,json,re

asset = sys.argv[1]

date_format = r"%Y%m%d"

dimensions = ['asset']
metrics = ['displays', 'uniq_displays', 'plays_requested', 'uniq_plays_requested']
headers = ['start_date', 'end_date'] + dimensions + metrics

def write_to_csv(data, str_date):
    path_csv = r'D:\ooyala_data'
    if not os.path.exists(path_csv): os.makedirs(path_csv)
    path = os.path.join(path_csv,'%s.csv'%str_date)
    file = open(path, 'wb')
    dict_writer = csv.DictWriter(file, headers, delimiter=';')
    dict_writer.writeheader()
    dict_writer.writerows(data)

def save_data(assets, str_date):
    data = []
    for asset in assets:
        results = asset.get('results', [])
        for result in results:
            for d in result.get('data', []):
                dates = {
                    'start_date': datetime.strptime(result['start_date'],'%Y-%m-%dT%H:%M:%S').strftime(date_format), 
                    'end_date': datetime.strptime(result['end_date'],'%Y-%m-%dT%H:%M:%S').strftime(date_format)
                }
                group = {k: d['group'][k] for k in dimensions}
                _metrics = {k: d['metrics'][k] for k in metrics}
                dates.update(group)
                dates.update(_metrics)
                data.append(dates)
    if(data):
        write_to_csv(data, str_date)

start_date = '2018-02-01'
end_date = '2018-05-21'

ooyala = Ooyala()


parameters = {
    'report_type': "performance", 
    'dimensions': ",".join(dimensions),
    'metrics':  ",".join(metrics),
    'start_date': start_date,
    'end_date': end_date,
    'filters' : 'asset=="%s"'%asset,
    'time_segment': 'month',
    'limit' :1000,
    'page' : 0
}

try:
    assets = ooyala.api.get('assets/'+asset)
    save_data([assets], start_date)
except Exception as ex:
    print(ex.message)


