# -*- coding: utf-8 -*-
from libraries.OoyalaAPI import OoyalaAPI

import os
import io
import sys
import logging
import json
import re
import time
from datetime import date, datetime, timedelta

from libraries.Common import Common
from libraries.Log import Log
from libraries.WritterCsv import WritterCsv

class Ooyala(Common):

    api = None

    dimensions = ['asset', 'device_type', 'country']
    metrics = ['displays', 'uniq_displays', 'plays_requested', 'uniq_plays_requested', 'replays',
               'video_starts', 'playthrough_25', 'playthrough_50', 'playthrough_75',
               'playthrough_100']
    headers = ['start_date', 'end_date'] + dimensions + metrics

    parameters = {
        'report_type': "performance",
        'dimensions': ",".join(dimensions),
        'metrics':  ",".join(metrics),
        'start_date': None,
        'end_date': None,
        'time_segment': 'day',
        'limit': 1000,
        'page': 0
    }

    def __init__(self):
        super(Ooyala, self).__init__()

        api_key = self.config.api.key
        secret_key = self.config.api.secret
        self.api = OoyalaAPI(api_key, secret_key)

        self.path_csv = os.path.join(self.config.data_path, 'ooyala')

        self.date_format = self.config.date_format
        self.data_downloaded = 0


    def get_data(self, current_date):

        path = os.path.join(self.path_csv, current_date+'.csv')

        if self.force_level >= 2 or not os.path.isfile(path):
            result = self.get_part(current_date)
            if not result['total_count']:
                return False
            
            csv = WritterCsv(path, self.headers, ';')
            verify_count = self.save_data(csv, result, current_date)

            # Verificar si hay mas páginas para descargar
            total_count = result['total_count']
            result_count = result['result_count']
            pages = ((total_count//result_count) if (total_count % result_count > 0)
                     else ((total_count//result_count) - 1)) if result_count > 0 else 0
            i = 1
            while i <= pages:
                asset_part = self.get_part(current_date, i, pages)
                verify_count += self.save_data(csv, asset_part, current_date)
                i = i+1

            if verify_count != total_count:
                logging.warn("No se obtuvieron la misma cantidad de registros (total: %d). Los registros indicados por la primera parte fueron %d." % (
                    verify_count, total_count))
            else:
                logging.info(
                    "Se obtuvieron %d registros de la api de Ooyala." % verify_count)
            csv.close()
            self.data_downloaded = verify_count
        else:
            logging.info(
                u"El archivo %s ya existe no será descargado." % (path))
            self.data_downloaded = 0
        return True

    def get_part(self, current_date, page=0, total_pages=0):
        part_name = 'page_%s.json' % page
        path_part = os.path.join(self.path_csv, current_date)
        part_file = os.path.join(path_part, part_name)

        success = False
        tries = 0
        assets = {}

        while not success or tries > 5:
            tries += 1
            if self.force_level >= 3 or not os.path.isfile(part_file):
                start_date = current_date

                start_day = datetime.strptime(start_date, self.date_format)
                end_date = (start_day+timedelta(days=1)
                            ).strftime(self.date_format)

                try:
                    logging.info("Descargando parte %s page %d/%d" %
                                 (current_date, page, total_pages))
                    self.parameters.update({
                        'start_date': start_date,
                        'end_date': end_date,
                        'page': page
                    })
                    assets = self.api.get('analytics/reports', self.parameters)

                    if not os.path.exists(path_part):
                        os.makedirs(path_part)

                    with open(part_file, 'wb') as f:
                        json.dump(assets, f)
                        success = True
                        self.data_downloaded += len(assets['results'] if assets['results'] else [])
                except Exception as ex:
                    logging.error('Error al obtener la data: %s' % ex.message)
            else:
                logging.info("Usando la parte json existente %s page %d/%d" %
                             (current_date, page, total_pages))
                with open(part_file, 'rb') as f:
                    assets = json.load(f)
                    success = True
            if 'results' not in assets:
                logging.info(
                    u"No se obtuvo resultado en la respuesta, se reintentará: %s" % json.dumps(assets))
                if os.path.isfile(part_file):
                    os.unlink(part_file)
                success = False
                time.sleep(60)
            if(tries >= 60):
                Log.error(u"No se pudo descargar la parte %s de la página %d/%d" %
                          (current_date, page, total_pages))

        if(assets):
            return assets
        else:
            # quit('Error al obtener los datos')
            return None

    def save_data(self, csv, asset, str_date):
        results = asset['results']
        count = 0
        outdates = 0
        for result in results:
            for d in result['data']:
                start_date = datetime.strptime(
                    result['start_date'], '%Y-%m-%dT%H:%M:%S').strftime(self.date_format)

                # Comprobar que el registro se encuentre en la fecha solicitada
                if start_date != str_date:
                    outdates += 1
                    continue
                dates = {
                    'start_date': start_date,
                    'end_date': datetime.strptime(result['end_date'], '%Y-%m-%dT%H:%M:%S').strftime(self.date_format)
                }
                group = {k: d['group'][k] for k in self.dimensions}
                metrics = {k: d['metrics'][k] for k in self.metrics}
                dates.update(group)
                dates.update(metrics)
                csv.writerow(dates)
                count += 1

        if outdates:
            logging.warn(u'Se encontraron %d registros que no pertenecían a la fecha solicitada' % outdates)

        return count

    def upload_data(self, current_date):

        path = os.path.join(self.path_csv, current_date+'.csv')
        table_name = self.config.psql.table

        fieldnames = ",".join(self.headers)
        where = {'start_date' : current_date}
        count = self.db.count(table_name, where = where) 
        
        if self.force_level >= 1 or count <= 0 or self.data_downloaded > 0 or count < WritterCsv(path, self.headers, ';').count_lines:
            if count > 0:
                deletes = self.db.delete(table_name, where = where)
            
            logging.info("Subiendo %s a DB postgres..." % (path))
            copies = self.db.upload_csv(table_name, path, delimiter=';', header=True)
            
            # Incrementar el contador de datos totales copiados
            self.psql_added += copies
        else:
            logging.info(
                "Actualmente la base de datos cuenta con %s registros que no fueron actualizados." % (count))

    def run(self, start, end, brand=''):
        
        # Verificar la fecha mínima
        min_start = datetime.strptime("2015-12-27", '%Y-%m-%d') 
        if start < min_start:
            start = min_start
            logging.warn(u"Se reemplazó la fecha mínima de datos existentes en Ooyala: 2015-12-27")
            
        diff = end - start
        days = diff.days+1
        for i in range(days):
            current_date = start+timedelta(days=i)
            str_date = current_date.strftime(self.date_format)
            logging.info("Iniciando proceso para la fecha %s..." % str_date)
            if self.get_data(str_date):
                self.upload_data(str_date)
            logging.info(
                "El proceso para la fecha %s ha terminado." % str_date)
