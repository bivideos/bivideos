# -*- coding: utf-8 -*-

import logging
import os
import csv
import sys
import gzip
import re
from datetime import date, timedelta, datetime
from libraries.Common import Common
from libraries.Log import Log

try:
    from googleads import dfp
    from googleads import errors
except Exception, ex:
    Log.error(u"No se encontró la librería googleads.")


class Dfp(Common):

    dimensions = ['AD_UNIT_NAME', 'DATE', 'CREATIVE_TYPE']
    columns = ['TOTAL_CODE_SERVED_COUNT', 'TOTAL_LINE_ITEM_LEVEL_IMPRESSIONS']
    headers = ['date', 'brand', 'total_code', 'total_impressions']

    def __init__(self):
        super(Dfp, self).__init__()

        self.path_csv = os.path.join(self.config.data_path, 'dfp')

        self.date_format = self.config.date_format
        self.report_downloaded = False
        self.data_formated = False

    # Leer el archivo de configuración de preroll-marcas
    def get_preroll_brands(self):

        content = self.resources.get('preroll_marcas.csv')

        products_data = [l.split(";") for l in content.split("\n")]

        preroll_brands = {}
        for row in products_data:
            preroll_brands[row[0]] = row[1]

        return preroll_brands

    def get_file_path(self, start_date, end_date, gzip=False):
        file_out_name = '%s.csv%s' % (start_date if start_date == end_date else (
            '%s-%s' % (start_date, end_date)), '.gz' if gzip else '')
        path = os.path.join(self.path_csv, file_out_name)
        return path

    def download_report(self, start_date, end_date):

        report_file_name = self.get_file_path(start_date, end_date, True)

        if self.force_level >= 3 or not os.path.isfile(report_file_name):

            # Ruta completa del archivo googleads.yaml o vacio si se encuentra en ~
            client = dfp.DfpClient.LoadFromStorage(
                os.path.join(self.config.get_path, 'googleads.yaml'))
            report_downloader = client.GetDataDownloader(version='v201802')
            logging.info("Downloading report %s-%s..." %
                         (start_date, end_date))

            start = datetime.strptime(start_date, self.date_format).date()
            end = datetime.strptime(end_date, self.date_format).date()
            payload = {
                'reportQuery': {
                    'dimensions': self.dimensions,
                    'columns': self.columns,
                    'dateRangeType': 'CUSTOM_DATE',
                    'startDate': start,
                    'endDate': end
                }
            }
            try:
                report_job_id = report_downloader.WaitForReport(payload)
            except Exception as e:
                Log.error('Error: %s' % e.message)

            if not os.path.exists(self.path_csv):
                os.makedirs(self.path_csv)

            report_file = open(report_file_name, 'wb')
            report_downloader.DownloadReportToFile(
                report_job_id, 'CSV_DUMP', report_file)
            logging.info('Report %s downloaded to %s' %
                         (report_job_id, report_file.name))
            report_file.close()
            self.report_downloaded = True
        else:
            logging.info(u"El archivo %s ya existe no será descargado." %
                         (report_file_name))
            self.report_downloaded = False

        result = []

        gzip_downloaded = gzip.open(report_file_name, "rb")
        reader_csv = csv.reader(gzip_downloaded)
        for row in reader_csv:
            result.append(row)
        gzip_downloaded.close()

        return result

    def format_data(self, assets):
        # Consolidar la data de los preroll agrupándolos por marca
        preroll_brands = self.get_preroll_brands()

        preroll_nonexistent = []
        resume_data = {}
        for row in assets:  # por cada fila en el resporte descargado
            # Solamente incluir los que tengan el tag preroll"
            if not re.search('preroll', row[0], re.IGNORECASE):
                continue

            creative_type = row[2]
            # Solamente aceptar Video creative sets
            if creative_type != 'Video creative sets':
                continue

            preroll = row[0]
            rdate = row[1]
            total_code = int(row[4])
            total_impressions = int(row[5])

            if preroll not in preroll_brands:
                if preroll not in preroll_nonexistent:
                    preroll_nonexistent.append(preroll)
                    logging.info(
                        u"Advertencia: El preroll %s no ha sido asignado a ninguna marca se omitirá" % preroll)
                continue

            brand = preroll_brands[preroll]

            if rdate not in resume_data:
                resume_data[rdate] = {}
            if brand not in resume_data[rdate]:
                resume_data[rdate][brand] = {
                    'total_code': total_code, 'total_impressions': total_impressions}
            else:
                resume_data[rdate][brand]['total_code'] += total_code
                resume_data[rdate][brand]['total_impressions'] += total_impressions

        data = list()
        for rdate, row in resume_data.items():
            for brand, totals in row.items():
                new_row = [rdate, brand, totals['total_code'],
                           totals['total_impressions']]
                data.append(new_row)
        return data

    def save_data(self, assets, start_date, end_date):
        full_filename = self.get_file_path(start_date, end_date)

        if self.force_level >= 2 or self.report_downloaded or not os.path.isfile(full_filename):
            data = self.format_data(assets)
            # Crear un nuevo csv donde se guardará la data consoldiada

            with open(full_filename, 'wb') as _f:
                writer = csv.writer(_f)
                writer.writerow(self.headers)
                writer.writerows(data)
                _f.close()
                logging.info("Archivo guardado: %s" % full_filename)

            self.data_formated = len(data)
        else:
            logging.info(
                u"El archivo %s ya existe no será descargado." % (full_filename))
            self.data_formated = False

    def upload_data(self, start_date, end_date):

        path = self.get_file_path(start_date, end_date)
        table_name = self.config.psql.table
        where = {
            'start_date[>=]' : start_date,
            'start_date[<=]' : end_date
        }
        count = self.db.count(table_name, where = where) 
        # Subir la data a la BD si se cumple una de las siguientes condiciones:
        # El nivel de fuerza se ha establecido como mayor o igual a 1 (self.force_level)
        # Previamente se ha descargado y formateado nueva data (self.data_formated)
        # No existe ningun registro actualmente en la BD (count)
        if self.force_level >= 1 or self.data_formated or count <= 0:

            logging.info("Subiendo %s a DB postgres..." % (path))

            if count > 0:
                deletes = self.db.delete(table_name, where = where)

            copies = self.db.upload_csv(table_name, path, delimiter=',', header=True)

            if self.data_formated > 0 and (self.data_formated != copies):
                logging.info("Advertencia: No se copiaron la misma cantidad de datos que se formatearon: %d vs %d" % (
                    copies, self.data_formated))

            if count > 0 and (deletes != copies):
                logging.info("Advertencia: No se copiaron la misma cantidad de datos que se eliminaron: %d vs %d" % (
                    copies, deletes))
        else:
            logging.info(
                "Actualmente la base de datos cuenta con %s registros que no fueron actualizados." % (count))

    def run(self, start, end, brand=''):

        # Verificar la fecha mínima
        min_start = datetime.now() - timedelta(days=3*365)
        if start < min_start:
            start = min_start
            logging.warn(u"Se reemplazó la fecha mínima de datos existentes en Ooyala: %s" % min_start)
        
        start_date = start.strftime(self.date_format)
        end_date = end.strftime(self.date_format)

        logging.info("Iniciando proceso para las fechas %s-%s..." %
                     (start_date, end_date))
        assets = self.download_report(start_date, end_date)
        self.save_data(assets, start_date, end_date)
        self.upload_data(start_date, end_date)
        logging.info("El proceso para las fechas %s-%s ha terminado." %
                     (start_date, end_date))
