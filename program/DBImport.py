# -*- coding: utf-8 -*-
from libraries.Log import Log

import os
import io
import logging
import re
from datetime import date, timedelta, datetime

from libraries.Common import Common
from libraries.SubProcess import SubProcess
from libraries.WritterCsv import WritterCsv


class DBImport(Common):

    products_csv = None

    def __init__(self):
        super(DBImport, self).__init__()
        self.path_csv = os.path.join(self.config.data_path, 'dbimport')
        self.date_format = self.config.date_format
        self.file_queries_path = os.path.join(self.resources.get_path(),
                                              'queries')
        self.data_downloaded = False
        self.table_name = self.config.psql.table

    def get_products(self, brand):
        if not self.products_csv:
            self.products_csv = self.resources.get('products.csv')
        products = "UNION ALL ".join(
            "SELECT '%s'brand,'%s'type,'%s'slug,'%s'nombre " %
            tuple(l.split(';')) for l in self.products_csv.split("\n"))
        return products

    def get_file_path(self, start_date, end_date, brand):
        file_out_path = os.path.join(self.path_csv, brand)
        if not os.path.exists(file_out_path):
            os.makedirs(file_out_path)

        if isinstance(start_date, datetime):
            start_date = start_date.strftime(self.date_format)

        if isinstance(end_date, datetime):
            end_date = end_date.strftime(self.date_format)

        file_out_name = '%s.csv' % (start_date if start_date == end_date else
                                    ('%s-%s' % (start_date, end_date)))
        path = os.path.join(file_out_path, file_out_name)
        return path

    def get_data(self, brand, start_date, end_date):
        # Ruta completa del archivo donde se descargará la data
        path = self.get_file_path(start_date, end_date, brand)

        # Descargar la data si se cumple una de las siguientes condiciones
        # Si el atributo fuerza se ha establecido con un valor mayor o igual a 3
        # Si El archivo no existe en el disco local
        if self.force_level >= 3 or not os.path.isfile(path):

            # Leer la configuración para conectarse a cada base de datos
            brands = self.config.brands
            opts = brands[brand]
            groups = self.config.groups
            group = groups[opts['group']]
            products = self.get_products(brand)

            file_query_name = 'query_%s.sql' % (
                'query' in opts and opts['query'] or opts['group'])
            query = self.resources.get(
                os.path.join('queries', file_query_name))
            for find, replace in {
                    '@brand': brand,
                    '@products': products,
                    '@start-date': start_date.strftime(self.date_format),
                    '@end-date': end_date.strftime(self.date_format)
            }.items():
                query = query.replace(find, replace)

            params = (
                'mysql',
                '-h%s' % group['host'],
                '-P%s' % group['port'],
                '-u%s' % group['user'],
                '-p%s' % group['password'],
                '-D%s' % opts['dbname'],
                "-s",
                '-N',  # --skip-column-names
                '-r',
                "--binary-mode",
                "--default-character-set=utf8",
                # '-e %s' % query,
            )
            try:
                logging.info("Descargando %s para las fechas %s-%s..." %
                             (brand, start_date.strftime(self.date_format), end_date.strftime(self.date_format)))
                csv = WritterCsv(path, delimiter='\t')
                SubProcess.call(
                    params, stdin=query.encode('utf-8'), stdout=csv.file_tmp)
                csv.close()
                logging.info('Se descargaron %d registros a %s' %
                             (csv.count_lines, csv.full_path))
                self.data_downloaded = True
            except Exception as ex:
                Log.error('No se pudo ejecutar: %s' % str(ex))
        else:
            logging.info(
                u"El archivo %s ya existe no será descargado." % (path))
            self.data_downloaded = False

    def validate_brands(self, brands):

        # Validar las marcas solicitadas
        if not 'brands':
            Log.error(u'No se envió ninguna marca.')
        elif brands[0] == 'all':
            brands = self.config.brands.keys()
        else:
            for brand in brands:
                if brand not in self.config.brands.keys():
                    logging.info('Marca no permitida: %s.' % brand)
                    brands.remove(brand)
        return brands

    def upload_data(self, brand, start_date, end_date):
        path = self.get_file_path(start_date, end_date, brand)
        f = open(path, mode="rb")
        lines = f.readlines()
        f.close()
        if not lines:
            logging.info("No se encontraron registros en el archivo %s para agregar a la base de datos."% path)
            return

        table_name = self.config.psql.table
        where = {
            'brand|upper': brand.upper(),
            'first_published_at[>=]': start_date.strftime(self.date_format),
            'first_published_at[<=]': end_date.strftime(self.date_format)
        }
        count = self.db.count(table_name, where=where)

        # Subir la data a la base de datos postgresql si cumple alguna condición:
        # Si se estableció el nivel de fuerza de actualización mayor o igual a 1
        # Si actualmente no existe ningun dato en la BD para la marca y fecha especificada
        # Si en el presente proceso se descargo nueva data de los origenes (mysql de md y xalok)
        if self.force_level >= 1 or count <= 0 or self.data_downloaded:

            # Eliminar data si actualmente existe alguna
            deletes = self.db.delete(
                table_name, where=where) if count > 0 else 0

            # Copiar nueva data del csv a la BD postgres
            copies = self.db.upload_csv(table_name, path)

            # Incrementar el contador de datos totales copiados
            self.psql_added += copies

            # Verificar si se eliminó data y se copia nueva, coincidan en cantidad o informar si no coincide
            if count > 0 and deletes != copies:
                logging.warn(
                    "La cantidad de datos que se copiaron (%s) fue diferente a la cantidad de datos que se eliminaron (%s), la data puede haber cambiado."
                    % (copies, deletes))
            else:
                logging.info(
                    "Se eliminaron %s registros y se copiaron %s nuevos." %
                    (deletes, copies))
        else:
            logging.info(
                "Actualmente la base de datos cuenta con %s registros que no fueron actualizados."
                % (count))

    def find_best_path(self, brand, start_date, end_date):
        # print('find_best_path', start_date, end_date)
        folder = os.path.join(self.path_csv, brand)
        list_files = os.listdir(folder)

        # Si existe el rango, devolver inmediatamente
        if '%s-%s.csv' % (start_date.strftime(self.date_format),end_date.strftime(self.date_format)) in list_files:
            return (start_date,end_date,end_date+timedelta(1))
        
        

        # Buscar el archivo más próximo
        exists = False
        end = end_date
        # Mientras no existe un archivo con las mejores fechas, buscar
        while not exists and end>=start_date:
            # Obtener el path del posible archivo que pueda existir
            exists = '%s-%s.csv' % (start_date.strftime(self.date_format),end.strftime(self.date_format)) in list_files
            # Si no existe buscar un dia antes
            if not exists:
                end -= timedelta(1)

        # print("Exists=%s" % exists)
        # Si no encontró ningun archivo, buscar la fecha fin mas cercana
        if not exists:
            end = start_date
            found_range = False
            found_single = False
            while end <= end_date:
                # print("Buscando %s" % end.strftime(self.date_format))
                r = re.compile(r"^%s-.*\.csv$" % end.strftime(self.date_format))
                newlist = filter(r.match, list_files)
                if newlist:# Se encontró una coincidencia de rango
                    found_range = True
                    # print('found_range')
                    break
                elif ('%s.csv'%end.strftime(self.date_format)) in list_files: # Si el archivo existe solo
                    found_single = True
                    # print('found_single')
                    break
                end += timedelta(1)
            if found_single:
                if end>start_date:
                    end -= timedelta(1)
            elif found_range:
                end -= timedelta(1)
            elif end > end_date:
                end = end_date
            
            
        next_start = (end+timedelta(1))
        return (start_date,end,next_start)


    def run(self, start_date, end_date, brand=''):
        brands = self.validate_brands(brand.split(','))
        logging.info("Iniciando proceso para las fechas %s-%s..." %
                     (start_date.strftime(self.date_format), end_date.strftime(self.date_format)))

        for brand in brands:
            logging.debug("Marca seleccionada: %s." % brand)

            next_start = start_date
            while next_start <= end_date:
                new_start, new_end, next_start = self.find_best_path(brand, next_start, end_date)
                logging.info("Procesando para las fechas %s a %s" % (new_start.strftime(self.date_format), new_end.strftime(self.date_format)))
                self.get_data(brand, new_start, new_end)
                self.upload_data(brand, new_start, new_end)

        logging.info("El proceso para las fechas %s-%s ha terminado." %
                     (start_date.strftime(self.date_format), end_date.strftime(self.date_format)))
