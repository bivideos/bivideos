# -*- coding: utf-8 -*-

from libraries.Common import Common
from datetime import date, datetime, timedelta
import csv,os,io,sys,logging,pickle,json,re
from libraries.Log import Log

class Resume(Common):
    
    def run(self, start, end, brand = ''):
        date_format = '%Y-%m-%d'
        start_date = start.strftime(date_format)
        end_date = end.strftime(date_format)

        logging.info("Resumiendo todas las tablas...")

        result = self.db.call_func('refresh_all_tables')
        logging.info("Se resumieron %d registros a la base de datos." % result)

        