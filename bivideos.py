﻿# -*- coding: utf-8 -*-
from libraries.Log import Log
import sys
import os
import time
import logging
from datetime import date, datetime, timedelta

date_format = r"%Y%m%d"



def main():
    # Establecer los argumentos
    from libraries.Argument import Argument
    args = Argument()
    args.add_argument('--process', help='Seleccionar procesos a ejecutar. Permitidos: dbimport,dfp,ooyala,resume. Predeterminadamente ejecuta todos los procesos.',
                                 type=str, required=False, default='all')

    # Seleccionar los procesos
    if args.process == 'all':
        process = ['dbimport', 'dfp', 'ooyala', 'resume']
    else:
        process = args.process.split(',')

    process_to_run = []
    for p in process:
        if p == 'dbimport':
            from program.DBImport import DBImport
            process_to_run.append(DBImport())
        elif p == 'dfp':
            from program.Dfp import Dfp
            process_to_run.append(Dfp())
        elif p == 'ooyala':
            from program.Ooyala import Ooyala
            process_to_run.append(Ooyala())
        elif p == 'resume':
            from program.Resume import Resume
            process_to_run.append(Resume())

    if not process_to_run:
        Log.error(u"No se selecciono ningún proceso.")

    try:
        for proc in process_to_run:
            proc.run(args.start_date, args.end_date, args.brand)
    except KeyboardInterrupt as ex:
        logging.warn("El proceso ha sido cancelado.")
        exit(1)
    except Exception as ex:
        Log.error(ex)


if __name__ in ('__main__', 'bivideos'):
    main()
    exit(0)
