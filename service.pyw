# -*- coding: utf-8 -*-

import time
import subprocess
from datetime import date, datetime, timedelta
import os


BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def run_bivideos():
    try:
        params = (
            'python',
            BASE_DIR
        )
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        proc = subprocess.Popen(
            params, stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=startupinfo)
        stdout, stderr = proc.communicate()
        if proc.returncode != 0:
            raise Exception("%s: Se produjo un error al ejecutar el proceso:\nError: %s" % (
                datetime.now(), stderr))
        return True
    except Exception as ex:
        print('No se pudo ejecutar: %s\nError:%s' %
              (" ".join(params), str(ex)))
    return False


def wait_for(seconds=None):
    if not seconds:
        now = datetime.now()
        midnight = datetime.combine(
            datetime.now()+timedelta(1), datetime.min.time())
        delta = midnight-now
        seconds = delta.total_seconds()+3600  # 1 hora luego de la media noche
    print('Esperando %d segundos.' % seconds)
    time.sleep(seconds)


def main():
    execute_days = list()

    # Ejecutar siempre
    while True:
        today = date.today()
        yesterday = (today - timedelta(1))

        if not yesterday in execute_days:
            run = run_bivideos()
            if run:
                execute_days.append(yesterday)
            else:
                wait_for(600)  # Reintentar en 10 min
        else:
            wait_for()


if __name__ == '__main__':
    main()
    exit(0)
