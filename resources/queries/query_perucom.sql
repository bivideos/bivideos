SELECT
	'@brand' as brand,
	page.nid as nota_id,
	FROM_UNIXTIME(page.modtime_edit) as updated_at,
	DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') as first_published_at,
	IF(pt.pid IS NULL,0,pt.pid) as category_id,
	IF(pt.portada IS NULL,'Ninguna',pt.portada) as category,
	IF(1=1,NULL,0) as producto,
	v.media_id as media_id,
	CAST( CASE p.origen 
		WHEN 0 THEN 'Ninguno'  WHEN 1 THEN 'Perú.com' 
		WHEN 2 THEN 'América TV' WHEN 3 THEN 'TNP' 
		WHEN 4 THEN 'Deutsche Welle' WHEN 5 THEN 'Voz de América' 
		WHEN 6 THEN 'RT' WHEN 7 THEN 'Estado peruano' WHEN 8 THEN 'Red Bull' 
		WHEN 9 THEN 'Cortesía' ELSE p.origen END AS CHAR CHARACTER SET utf8) as video_origin,
	IF(p.publicidad>0,1,NULL) as video_publicidad
FROM
	md_noticias page
LEFT JOIN (
	SELECT
		ne.nid,
		ne.fid,
		s.media_id
	FROM md_noticias_elementos ne
	INNER JOIN md_sambatech_media s ON s.fid = ne.fid
	WHERE LENGTH(s.media_id)>0
) v on v.nid = page.nid
LEFT JOIN md_elementos_publicidad p ON p.fid = v.fid
LEFT JOIN md_noticias_categoria nc ON nc.nid = page.nid
LEFT JOIN md_portada_term pt ON pt.pid = nc.pid
WHERE
	page.publish = 1
 	AND DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') >= '@start-date'	
	AND DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') <= '@end-date'	