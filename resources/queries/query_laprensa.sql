SELECT
	'@brand' as brand,
	page.nid as nota_id,
	FROM_UNIXTIME(page.modtime_edit) as updated_at,
	DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') as first_published_at,
	c.cid as category_id,
	c.nombre as category,
	IF(1=1,NULL,0) as producto,
	noticia_video.media_id,
	IF(LENGTH(noticia_video.media_id)>1,'Ninguno',NULL) as video_origin,
	IF(LENGTH(noticia_video.media_id)>1,1,NULL) as video_publicidad
FROM
	md_noticias page
LEFT JOIN (
	SELECT 
		e.nid,
		s.fid,
		s.media_id
	FROM md_noticias_elementos e
	INNER JOIN md_sambatech_media s  on e.fid = s.fid
) noticia_video ON noticia_video.nid = page.nid
LEFT JOIN md_noticias_categoria nc ON  nc.nid = page.nid
LEFT JOIN md_categoria c ON  c.cid = nc.cid
WHERE
	page.publish = 1 
	AND DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') >= '@start-date' 
	AND DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') <= '@end-date' 