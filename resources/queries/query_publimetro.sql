SELECT
	'@brand' as brand,
	page.nid as nota_id,
	FROM_UNIXTIME(page.modtime_edit) as updated_at,
	DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') as first_published_at,
	c.cid as category_id,
	c.nombre as category,
	IF(1=1,NULL,0) as producto,
	noticia_video.media_id as media_id,
	CAST(CASE e.rptOrigen 
			WHEN 0 THEN '-- Todos --'  WHEN 1 THEN 'Extern - AFP' WHEN 2 THEN 'Propio - Captura de TV'
			WHEN 6 THEN 'Extern - REUTERS' WHEN 7 THEN 'Propio - Celular'
			ELSE e.rptOrigen END AS CHAR CHARACTER SET utf8) as video_origin,
	IF(e.publicidad>0,1,NULL) as video_publicidad
FROM
	md_noticias page
LEFT JOIN (
	SELECT 
		ne.nid,
		s.fid,
		s.media_id
	FROM md_noticias_elementos ne 
	INNER JOIN md_sambatech_media s ON s.fid = ne.fid
	WHERE s.media_id is not null and s.media_id <> ''
) noticia_video ON noticia_video.nid = page.nid
LEFT JOIN md_elementos e ON noticia_video.fid = e.fid
LEFT JOIN md_noticias_categoria nc ON nc.nid = page.nid
LEFT JOIN md_categoria c ON c.cid = nc.cid
WHERE
	page.publish = 1 
 	AND DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') >= '@start-date'
	AND DATE_FORMAT(FROM_UNIXTIME(page.modtime_created),'%Y-%m-%d') <= '@end-date'
	