SELECT
	'@brand' as brand,
	page.id as nota_id,
	IF(page.updated_at is null,page.created_at,page.updated_at) as updated_at,
	DATE_FORMAT(page.first_published_at,'%Y-%m-%d') as first_published_at,
	categorias.category_id as category_id,
	categorias.category as category,
	page_producto.nombre as producto,
	page_video.media_id as media_id,
	page_video.origin as video_origin,
	IF(page_video.video_id>0,IF(video_noads.noads>0,NULL,1),NULL) as video_publicidad
FROM
	page
LEFT JOIN (
	SELECT
		page.id AS page_id,
		video.id as video_id,
		video.media_id,
		video.origin
	FROM page
	INNER JOIN (
		SELECT
			(t * 10 + u) digit
		FROM
			(SELECT 0 t UNION SELECT 1 UNION SELECT 2) A, 
			(SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) B
			ORDER BY digit
		) n 
		ON LENGTH(REPLACE (page.videos, ',', '')) <= LENGTH(page.videos) - n.digit
	INNER JOIN video on video.id = REPLACE(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(page.videos, ',', n.digit + 1),',',- 1), ']', ''),'[','') AND video.source in ('local_ooyala','ooyala')
 	WHERE 1=1
	 	AND DATE_FORMAT(page.first_published_at,'%Y-%m-%d') >= '@start-date'
		AND DATE_FORMAT(page.first_published_at,'%Y-%m-%d') <= '@end-date'
) page_video ON page_video.page_id = page.id
LEFT JOIN (
		SELECT DISTINCT
			video_tag.video_id,
			1 AS noads
		FROM
			tag
		LEFT JOIN video_tag ON video_tag.tag_id = tag.id
		WHERE
			tag.slug = 'noads'
) video_noads on video_noads.video_id = page_video.video_id
LEFT JOIN (
	SELECT
		c1.id,
		IF(c3.parent_id,c3.id,IF(c2.parent_id,c2.id,c1.id)) as category_id,
		IF(c3.parent_id,c3.title,IF(c2.parent_id,c2.title,c1.title)) as category
	FROM	
		category c1
	LEFT JOIN category c2 ON c2.id = c1.parent_id
	LEFT JOIN category c3 ON c3.id = c2.parent_id
) categorias on categorias.id = page.category_id

LEFT JOIN 
	(
		SELECT DISTINCT id as page_id,nombre from (
				SELECT
					page.id,
					p.nombre
				FROM (SELECT brand,type,slug,nombre FROM (@products) productos WHERE brand = '@brand' and type = 'tag')  p
				LEFT JOIN tag ON tag.slug = p.slug
				LEFT JOIN page_tag ON page_tag.tag_id = tag.id
				LEFT JOIN page ON page.id = page_tag.page_id
				WHERE 1=1
					AND DATE_FORMAT(page.first_published_at,'%Y-%m-%d') >= '@start-date'
					AND DATE_FORMAT(page.first_published_at,'%Y-%m-%d') <= '@end-date'
		) as page_producto
	) page_producto ON page_producto.page_id = page.id
WHERE 
	page.page_type = 'article'
 	AND DATE_FORMAT(page.first_published_at,'%Y-%m-%d') >= '@start-date' 
	AND DATE_FORMAT(page.first_published_at,'%Y-%m-%d') <= '@end-date' 